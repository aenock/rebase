import React from 'react';
import MediaCard from '../MediaCard/MediaCard';

function MediaCardArea() {
    const data = [
        {
            component: "img",
            alt: "Pizza Margherita",
            height: "140",
            image: "/static/images/cards/pizza-margherita.jpg",
            title: "Pizza Margherita"
        },
        {
            component: "img",
            alt: "Pizza Margherita",
            height: "140",
            image: "/static/images/cards/pizza-margherita.jpg",
            title: "Pizza Margherita"
        }, {
            component: "img",
            alt: "Pizza Margherita",
            height: "140",
            image: "/static/images/cards/pizza-margherita.jpg",
            title: "Pizza Margherita"
        }, {
            component: "img",
            alt: "Pizza Margherita",
            height: "140",
            image: "/static/images/cards/pizza-margherita.jpg",
            title: "Pizza Margherita"
        }, {
            component: "img",
            alt: "Pizza Margherita",
            height: "140",
            image: "/static/images/cards/pizza-margherita.jpg",
            title: "Pizza Margherita"
        }, {
            component: "img",
            alt: "Pizza Margherita",
            height: "140",
            image: "/static/images/cards/pizza-margherita.jpg",
            title: "Pizza Margherita"
        },
    ]
    console.log('na data', data);
    return (
        <div>
            {data.map(function (item) {
                return <MediaCard className={'MediaCard'} data={item} />
            })
            }
        </div>
    );
}

export default MediaCardArea;
