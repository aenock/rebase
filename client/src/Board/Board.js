import React from 'react';
import ButtonTemplate from '../ButtonTemplate/ButtonTemplate';
import MediaCardArea from '../MediaCardArea/MediaCardArea';

function Board() {
  console.log('buda!');
  return (
    <div className="Board">
      <div className="BoardHeader">
        Menu
        <ButtonTemplate string="Add menu item" />
      </div>
      <div className="BoardContent">
        <MediaCardArea />
      </div>
    </div>
  );
}

export default Board;
