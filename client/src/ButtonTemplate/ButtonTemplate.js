import React from 'react';
import Button from '@material-ui/core/Button';

function ButtonTemplate(props) {
  return (
    <Button variant="contained" color="primary">
      {props.string}
    </Button>
  );
}

export default ButtonTemplate;
