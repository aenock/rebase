import React from 'react';
import Board from './Board/Board';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="row">
        <header className="App-header">
          <p style={{float:'left'}}>
            CAFE REACT
          </p>
        </header>
      </div>
      <Board />
    </div>
  );
}

export default App;
