import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    card: {
        maxWidth: 345,
    },
});


function MediaCard(props) {
    const classes = useStyles();
    console.log('mannnn', props);
    return (
        <Card className={'MediaCard'}>
            <CardActionArea>
                <CardMedia
                    component={props.data.component}
                    alt={props.data.alt}
                    height={props.data.height}
                    image={props.data.image}
                    title={props.data.title}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        Main Course
            </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        Pizza Margherita
            </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size="small" color="primary">
                    Share
          </Button>
                <Button size="small" color="primary">
                    Learn More
          </Button>
            </CardActions>
        </Card>
    );
}

export default MediaCard;
